﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a CodeSmith Template.
//
//     DO NOT MODIFY contents of this file. Changes to this
//     file will be lost if the code is regenerated.
//     Author:Yubao Li
// </autogenerated>
//------------------------------------------------------------------------------

using System.Collections.Generic;
using Infrastructure;
using OpenAuth.Domain;

namespace OpenAuth.App.ViewModel
{
    /// <summary>
	/// 
	/// </summary>
    public class CommonApplyVM :Entity
    {
        /// <summary>
	    /// 
	    /// </summary>
        public int Sort { get; set; }
        /// <summary>
	    /// 
	    /// </summary>
        public int Number { get; set; }
        /// <summary>
	    /// 
	    /// </summary>
        public string Name { get; set; }
        /// <summary>
	    /// 
	    /// </summary>
        public string Comment { get; set; }
        /// <summary>
	    /// 
	    /// </summary>
        public string State { get; set; }
        /// <summary>
	    /// 
	    /// </summary>
        public string StateName { get; set; }
        /// <summary>
	    /// 
	    /// </summary>
        public System.Guid UserId { get; set; }
        /// <summary>
	    /// 
	    /// </summary>
        public System.Guid? ControllerUserId { get; set; }

        public string WorkflowName { get; set; }

        /// <summary>
        /// 可用命令
        /// </summary>
        public CommandModel[] Commands { get; set; }

        public Dictionary<string, string> AvailiableStates { get; set; }

        public static implicit operator CommonApplyVM(CommonApply obj)
        {
            return obj.MapTo<CommonApplyVM>();
        }

        public static implicit operator CommonApply(CommonApplyVM obj)
        {
            return obj.MapTo<CommonApply>();
        }

    }
}