﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenAuth.Domain.Interface
{
    public interface IWorkflowSchemeRepository : IRepository<WorkflowScheme>
    {
        

    }
}